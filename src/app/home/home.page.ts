import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  router: Router;

  constructor() {}

  informacionCompra: informacion;

  pasarParametro() {
    let navigationExtras: NavigationExtras = {
      state: {
        parametros: this.informacionCompra,
      }
    };
      
    this.router.navigate(['detalle'], navigationExtras);
  }
}

class informacion {
  codigoPromocional: String;
  email: String;
  consent: Boolean;
  nombre: String;
  apellido: String;
  direccionLinea1: String;
  direccionLinea2: String;
  pais: String;
  ciudad: String;
  provincia: String;
  cp: Number;
  telefono: Number;
  medotoEnvio: String;
  numTargeta: Number;
  nombreTargeta: String;
  mesExpiracion: Number;
  anoExpiracion: Number;
  cvv: Number;
  agree: Boolean;

  constructor() {}

  insertInfoPag1(lCodigoPromocional: String, lEmail: String, lNombre: String, lApellido: String, lDireccionLinea1: String, lDireccionLinea2: String, lPais: String, lCiudad: String, lProvincia: String, lCp: Number, lTelefono: Number) {
    this.codigoPromocional = lCodigoPromocional;
    this.email = lEmail;
    this.nombre = lNombre;
    this.apellido = lApellido;
    this.direccionLinea1 = lDireccionLinea1;
    this.direccionLinea2 = lDireccionLinea2;
    this.pais = lPais;
    this.ciudad = lCiudad;
    this.provincia = lProvincia;
    this.cp = lCp;
    this.telefono = lTelefono;
  }

  showInfoPersonal(): String {
    return "Información personal: " + this.nombre + this.apellido + ", " + this.email + ", " + this.telefono;
  }

  showInfoTargeta(): String {
    return "Información de targeta: " + this.numTargeta + ", " + this.nombreTargeta;
  }

  showInfoEnvio(): String {
    return "Envío: " + this.direccionLinea1 + ", " + this.cp + ", " + this.pais + ", " + this.provincia;
  }
}
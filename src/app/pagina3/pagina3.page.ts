import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-pagina3',
  templateUrl: './pagina3.page.html',
  styleUrls: ['./pagina3.page.scss'],
})
export class Pagina3Page implements OnInit {

  data: any;

  numTargeta: Number;
  nombreTargeta: String;
  mesExpiracion: Number;
  anoExpiracion: Number;
  cvv: Number;

  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.data = this.router.getCurrentNavigation().extras.state.parametros;
      }
    });
  }

  pasarParametro() {
    if (this.numTargeta === undefined || this.nombreTargeta === undefined || this.mesExpiracion === undefined || this.anoExpiracion === undefined || this.cvv === undefined) {
      alert("Falten camps obligatoris. ");
    } else {
      let navigationExtras: NavigationExtras = {
        state: {
          parametros: this.data,
        }
      };
      
      this.data.insertInfoPag3(this.numTargeta, this.nombreTargeta, this.mesExpiracion, this.anoExpiracion, this.cvv);
      this.router.navigate(['pagina4'], navigationExtras);
    }
  }

  ngOnInit() {
  }

}

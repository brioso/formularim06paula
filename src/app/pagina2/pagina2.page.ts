import { Component, OnInit } from '@angular/core';
import { NavigationExtras, ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-pagina2',
  templateUrl: './pagina2.page.html',
  styleUrls: ['./pagina2.page.scss'],
})
export class Pagina2Page implements OnInit {

  infoEnvio: String;
  data: any;

  constructor(private route: ActivatedRoute, private router: Router) {
    

    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.data = this.router.getCurrentNavigation().extras.state.parametros;
        this.infoEnvio = this.data.showInfoEnvio();
      }
    });
  }

  pasarParametro() {
    let navigationExtras: NavigationExtras = {
      state: {
        parametros: this.data,
      }
    };
      
    this.router.navigate(['pagina3'], navigationExtras);
  }

  ngOnInit() {
  }

}

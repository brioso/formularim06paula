import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-pagina4',
  templateUrl: './pagina4.page.html',
  styleUrls: ['./pagina4.page.scss'],
})
export class Pagina4Page implements OnInit {

  data: any;

  infoCliente: String;
  infoTargeta: String;
  infoEnvio: String;

  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.data = this.router.getCurrentNavigation().extras.state.parametros;
        this.infoCliente = this.data.showInfoPersonal();
        this.infoTargeta = this.data.showInfoTargeta();
        this.infoEnvio = this.data.showInfoEnvio();
      }
    });
  }

  ngOnInit() {
  }
}

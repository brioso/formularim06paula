import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-pagina1',
  templateUrl: './pagina1.page.html',
  styleUrls: ['./pagina1.page.scss'],
})
export class Pagina1Page implements OnInit {

  codigoPromocional: String;
  email: String;
  consent: String;
  nombre: String;
  apellido: String;
  direccionLinea1: String;
  direccionLinea2: String;
  pais: String;
  ciudad: String;
  cp: Number;
  provincia: String;
  telefono: Number;

  constructor(private router: Router) {
  }
  
  pasarParametro() {
    if (this.email === undefined || this.nombre === undefined || this.apellido === undefined || this.direccionLinea1 === undefined || this.pais === undefined || this.ciudad === undefined || this.cp === undefined || this.provincia === undefined || this.telefono === undefined) {
      alert("Falten camps obligatoris. ");
    } else {

      console.log("holi");
      class informacion {
        codigoPromocional: String = "";
        email: String = "";
        consent: String = "";
        nombre: String = "";
        apellido: String = "";
        direccionLinea1: String = "";
        direccionLinea2: String = "";
        pais: String = "";
        ciudad: String = "";
        provincia: String = "";
        cp: Number = 0;
        telefono: Number = 0;
        medotoEnvio: String = "";
        numTargeta: Number = 0;
        nombreTargeta: String = "";
        mesExpiracion: Number = 0;
        anoExpiracion: Number = 0;
        cvv: Number = 0;
        agree: String = "";
      
        constructor(lCodigoPromocional: String, lEmail: String, lNombre: String, lApellido: String, lDireccionLinea1: String, lDireccionLinea2: String, lPais: String, lCiudad: String, lProvincia: String, lCp: Number, lTelefono: Number) {
          this.codigoPromocional = lCodigoPromocional;
          this.email = lEmail;
          this.nombre = lNombre;
          this.apellido = lApellido;
          this.direccionLinea1 = lDireccionLinea1;
          this.direccionLinea2 = lDireccionLinea2;
          this.pais = lPais;
          this.ciudad = lCiudad;
          this.provincia = lProvincia;
          this.cp = lCp;
          this.telefono = lTelefono;
        }
      
        showInfoPersonal(): String {
          return "Información personal: " + this.nombre + " " + this.apellido + ", " + this.email + ", " + this.telefono;
        }
      
        showInfoTargeta(): String {
          return "Información de targeta: " + this.numTargeta + ", " + this.nombreTargeta;
        }
      
        showInfoEnvio(): String {
          return "Envío: " + this.direccionLinea1 + ", " + this.cp + ", " + this.pais + ", " + this.provincia;
        }

        insertInfoPag3(lNumTargeta: Number, lNombreTargeta: String, lMesExpiracion: Number, lAnoExpiracion: Number, lCvv: Number) {
          this.numTargeta = lNumTargeta;
          this.nombreTargeta = lNombreTargeta;
          this.mesExpiracion = lMesExpiracion;
          this.anoExpiracion = lAnoExpiracion;
          this.cvv = lCvv;
        }
      }

      let navigationExtras: NavigationExtras = {
        state: {
          parametros: new informacion(this.codigoPromocional, this.email, this.nombre, this.apellido, this.direccionLinea1, this.direccionLinea2, this.pais, this.ciudad, this.provincia, this.cp, this.telefono)
        }
      };
          
      this.router.navigate(['pagina2'], navigationExtras);
    }
  }

  ngOnInit() {
  }

}




